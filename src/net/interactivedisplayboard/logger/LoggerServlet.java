package net.interactivedisplayboard.logger;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = { "/logger" })
public class LoggerServlet extends HttpServlet
{
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //request.getRequestDispatcher("/WEB-INF/upload.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	String ip = request.getRemoteAddr();
    	
    	Log[] logs = (Log[])request.getAttribute("logs");
    	
    	for (Log log : logs)
    	{
    		Logger.log(log);
    	}
    }
}