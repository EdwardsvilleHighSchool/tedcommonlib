package net.interactivedisplayboard.logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import net.interactivedisplayboard.util.Location;

/**
 * Class used to log data to a log file on the server.
 * 
 * Date: 1/1/2014 5:00:00 PM
 */
public class Logger
{
	private static final String	LOG_FILENAME = "log.txt";
	
	private static final File	LOG_FILE;
	
	private static PrintWriter	writer;
	
	/**
	 * Find the log file.
	 */
	static
	{
		LOG_FILE = new File(Location.getControlPanelRoot() + "/" + LOG_FILENAME);
	}
	
	/**
	 * Create the connection with the log file used to write the
	 * data to.
	 */
	private static void openLogFile()
	{
		try
		{
			writer = new PrintWriter(new BufferedWriter(new FileWriter(LOG_FILE, true)));
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			
			throw new RuntimeException("Log file not found: " + e.getMessage());
		}
		catch (IOException e)
		{
			e.printStackTrace();
			
			throw new RuntimeException("An IO exception has occurred with the Log file: " + e.getMessage());
		}
	}
	
	/**
	 * Log a message to the log file.
	 * 
	 * @param status The status of the message to log to. Options
	 * 		include:<br>
	 * 		<ul>
	 * 			<li>Log.MESSAGE</li>
	 * 			<li>Log.ERROR</li>
	 * 			<li>Log.WARNING</li>
	 * 		</ul>
	 * @param ip The IP that the log is coming from.
	 * @param message The message to log to the file that explains
	 * 		what has happened.
	 */
	public static void log(int status, String ip, String message)
	{
		Log log = new Log(status, ip, message);
		
		log(log);
	}
	
	/**
	 * Log a message to the log file.
	 * 
	 * @param log The Log instance that contains all of the information
	 * 		of the log to submit.
	 */
	public static void log(Log log)
	{
		int    status  = log.getStatus();
		String ip      = log.getIP();
		String message = log.getMessage();
		
		if (status <= 0 || status > 3)
		{
			throw new RuntimeException("Status value of " + status + " is not a valid status.");
		}
		
		String statusValue = null;
		
		if (status == Log.MESSAGE)
		{
			statusValue = "MESSAGE";
		}
		else if (status == Log.WARNING)
		{
			statusValue = "WARNING";
		}
		else if (status == Log.ERROR)
		{
			statusValue = "ERROR";
		}
		
		// Get the current date and time.
		String timestamp      = null;
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy 'at' HH:mm:ss");
		Calendar   cal        = Calendar.getInstance();
		
		timestamp             = dateFormat.format(cal.getTime());
		
		ip                    = "(" + ip + ")";
		
		openLogFile();
		
		writer.println(statusValue + " " + ip + ": \"" + message + "\" on " + timestamp);
		
		writer.close();
	}
}