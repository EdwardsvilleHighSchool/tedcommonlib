package net.interactivedisplayboard.logger;

/**
 * Class used to store information for a log to be submitted.
 * 
 * Date: 1/2/2014 8:00:00 PM
 */
public class Log
{
	private int		status;
	
	private String	ip;
	private String	message;
	
	/**
	 * Status constant used when a log is describing something
	 * that is a typical procedure.
	 */
	public static final int	MESSAGE = 1;
	
	/**
	 * Status constant used when a log is describing something
	 * that has potential for failure.
	 */
	public static final int	WARNING = 2;
	
	/**
	 * Status constant used when a log is describing something
	 * that has failed in the interaction.
	 */
	public static final int	ERROR   = 3;
	
	/**
	 * Create a blank log. The information will be modified
	 * through the mutators.
	 */
	public Log()
	{
		this(0, null, null);
	}
	
	/**
	 * Create a log with the specified information attached to it.
	 * 
	 * @param status The status of the message to log to. Options
	 * 		include:<br>
	 * 		<ul>
	 * 			<li>Log.MESSAGE</li>
	 * 			<li>Log.ERROR</li>
	 * 			<li>Log.WARNING</li>
	 * 		</ul>
	 * @param ip The IP that the log is coming from.
	 * @param message The message to log to the file that explains
	 * 		what has happened.
	 */
	public Log(int status, String ip, String message)
	{
		setStatus(status);
		setIP(ip);
		setMessage(message);
	}
	
	/**
	 * Get the status of the message that will be logged.
	 * 
	 * @return The status of the message that will be logged.
	 */
	public int getStatus()
	{
		return status;
	}
	
	/**
	 * Set the status of the message that will be logged.
	 * 
	 * @param status The status of the message that will be logged.
	 */
	public void setStatus(int status)
	{
		this.status = status;
	}
	
	/**
	 * Get the IP that the log is coming from.
	 * 
	 * @return The IP that the log is coming from.
	 */
	public String getIP()
	{
		return ip;
	}
	
	/**
	 * Set the IP that the log is coming from.
	 * 
	 * @param ip The IP that the log is coming from.
	 */
	public void setIP(String ip)
	{
		this.ip = ip;
	}
	
	/**
	 * Get the message to log to the file that explains
	 * 		what has happened.
	 * 
	 * @return The message to log to the file that explains
	 * 		what has happened.
	 */
	public String getMessage()
	{
		return message;
	}
	
	/**
	 * Set the message to log to the file that explains
	 * 		what has happened.
	 * 
	 * @param message The message to log to the file that explains
	 * 		what has happened.
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}
	
	/**
	 * Get the status value by passing the label of the status.<br>
	 * <br>
	 * e.g. getStatus("warning") == Log.WARNING
	 * 
	 * @param label The label that describes the status.
	 * @return The constant value of the status.
	 */
	public static int getStatus(String label)
	{
		if (label.toLowerCase().equals("message"))
		{
			return MESSAGE;
		}
		else if (label.toLowerCase().equals("warning"))
		{
			return WARNING;
		}
		else if (label.toLowerCase().equals("error"))
		{
			return ERROR;
		}
		
		return 0;
	}
}