package net.interactivedisplayboard;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.interactivedisplayboard.database.DBConnection;

/**
 * Class that provides an interface for an HTTPServlet, as well as
 * implementations of two helper methods: forwardSuccess() and
 * forwardError() used to simplify the forwarding process.
 * 
 * Date: 2/26/2014 6:00:00 PM
 */
public class EHSServlet extends HttpServlet
{
	/**
     * Forward the servlet to the specified URL with the success status.
     * 
     * @param request The request variable of the servlet.
     * @param response The response variable of the servlet.
     * @param url The url to forward the servlet to.
     * @throws ServletException If an exception occurs while forwarding.
     * @throws IOException If an IOException occurs while forwarding.
     */
    public static void forwardSuccess(HttpServletRequest request, HttpServletResponse response, String url) throws ServletException, IOException
    {
    	RequestDispatcher requetsDispatcherObj;
    	
    	url = url.replace("\"", "\\\"");
    	
		request.setAttribute("url", url);
		requetsDispatcherObj = request.getRequestDispatcher("/jsp/loadPage.jsp");
		requetsDispatcherObj.forward(request, response);
    }
    
    /**
     * Forward the servlet to the specified URL with the error status.
     * 
     * @param request The request variable of the servlet.
     * @param response The response variable of the servlet.
     * @param url The url to forward the servlet to.
     * @param error The error message from the servlet.
     * @throws ServletException If an exception occurs while forwarding.
     * @throws IOException If an IOException occurs while forwarding.
     */
    public static void forwardError(HttpServletRequest request, HttpServletResponse response,  String url, String error) throws ServletException, IOException
    {
    	error = error.replace("\"", "&quot;");
    	
    	if (!url.endsWith(";"))
    	{
    		url += '#';
    	}
    	
    	url += "error=\"" + error + "\";";
    	
    	forwardSuccess(request, response, url);
    }
    
    /**
     * Output an error in a div format.
     * 
     * @param error The error message to output.
     * @param out The output stream used to output the div.
     * @throws IOException Thrown if there is an error outputting the div.
     */
    public static String outputError(String error) throws IOException
    {	
		return "<div id='output'>" +
						"{ " +
							"\"error\": \"" + error + "\" " +
						"}" +
					"</div>";
    }
    
    /**
     * Get the number of rows output from a query.
     * 
     * @param query The query to get the number of rows from.
     * @return The number of rows returned from the query.
     * @throws SQLException Thrown if there is a problem accessing the
     * 		number of rows.
     */
    public static int getNumRows(String query) throws SQLException
    {
    	DBConnection connection = DBConnection.getEhsDataConnection();
    	
    	ResultSet result = connection.query(query);
    	
    	return getNumRows(result);
    }
    
    /**
     * Get the number of rows in a ResultSet Object.
     * 
     * @param set The ResultSet to get the number of rows from.
     * @return The number of rows in the ResultSet Object.
     * @throws SQLException Thrown if there is a problem accessing the
     * 		number of rows.
     */
    public static int getNumRows(ResultSet set) throws SQLException
    {
    	int prev = set.getRow();
    	
    	set.last();
    	
    	int rows = set.getRow();
    	
    	set.absolute(prev);
    	
    	return rows;
    }
    
    /**
     * Get the maximum value of a column in a table.
     * 
     * @param columnName The column to get the max value of.
     * @param tableName The name of the table to get the value from.
     * @return The maximum value from the column in the specified table.
     * @throws SQLException Thrown if there was a problem retrieving the
     * 		data.
     */
    public static int getMax(String columnName, String tableName) throws SQLException
    {
		String       query      = "SELECT MAX(" + columnName + ") AS max FROM " + tableName + ";";
		DBConnection connection = DBConnection.getEhsDataConnection();
		ResultSet    results    = connection.query(query);
		
		results.first();
		
		return results.getInt("max");
    }
    
    /**
     * Get the IP address of the current client viewing the web-page.
     * 
     * @param request The HttpServletRequest containing the IP.
     * @return The String form of the IP address.
     */
    public static String getIP(HttpServletRequest request)
    {
    	return request.getRemoteAddr();
    }
    
    /**
     * Format the String to be put in the database correctly.
     * 
     * @param input The input String to format.
     * @return The formatted String.
     */
    public static String formatString(String input)
    {
    	String output = input;
    	
    	output = output.replace("'", "&#39;");
    	output = output.replace("\"", "&quot;");
    	
    	return output;
    }
}