package net.interactivedisplayboard;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.interactivedisplayboard.database.DBConnection;
import net.interactivedisplayboard.logger.Log;
import net.interactivedisplayboard.logger.Logger;

// The URL in which the servlet is located at. e.g. http://localhost/addEntry
@WebServlet(urlPatterns = { "/retrieveEntry" })
/**
 * Class used to get all of the entry data from the database.
 * 
 * Date: 2/24/2014 6:00:00 PM
 */
public class RetrieveEntryServlet extends EHSServlet
{
    /**
     * Method called whenever a get request has been made on the
     * upload servlet.
     * 
     * @param request The request information from the sender.
     * @param response The response information to send back.
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        request.getRequestDispatcher("/error404").forward(request, response);
    }
    
    /**
     * Method called whenever a post request has been made on the
     * retrieve entry servlet.<br>
     * <br>
     * Outputs all of the database timeline entries in JSON format.
     * 
     * @param request The request information from the sender.
     * @param response The response information to send back.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	ServletOutputStream out = response.getOutputStream();
    	
    	ArrayList<Integer> idList          = new ArrayList<Integer>();
    	ArrayList<String>  authorList      = new ArrayList<String>();
    	ArrayList<String>  descriptionList = new ArrayList<String>();
    	ArrayList<String>  titleList       = new ArrayList<String>();
    	ArrayList<String>  dateList        = new ArrayList<String>();
    	ArrayList<String>  timeList        = new ArrayList<String>();
    	ArrayList<String>  urlList         = new ArrayList<String>();
    	
    	DBConnection connection = DBConnection.getEhsDataConnection();
    	
    	try
		{
			ResultSet results = connection.query("SELECT * FROM timeline_data;");
			
			results.first();
			
			while (!results.isAfterLast())
			{
				int    id          = results.getInt("id");
				String author      = results.getString("author");
				String description = results.getString("description");
				String title       = results.getString("title");
				String date        = results.getString("date");
				String time        = results.getString("time");
				String url         = results.getString("url");
				
				results.next();
				
				idList.add(id);
				authorList.add(author);
				descriptionList.add(description);
				titleList.add(title);
				dateList.add(date);
				timeList.add(time);
				urlList.add(url);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			
			outputError("Unable to query database.");
			
			return;
		}
    	
    	int ids[] = new int[idList.size()];
    	
    	for (int i = 0; i < ids.length; i++)
    	{
    		ids[i] = idList.get(i);
    	}

    	String authors[]      = authorList.toArray(new String[0]);
    	String descriptions[] = descriptionList.toArray(new String[0]);
    	String titles[]       = titleList.toArray(new String[0]);
    	String dates[]        = dateList.toArray(new String[0]);
    	String times[]        = timeList.toArray(new String[0]);
    	String urls[]         = urlList.toArray(new String[0]);
    	
    	String output        = generateOutput(ids, titles, dates, times, urls, authors, descriptions);
    	
    	out.println(output);
    }
    
    /**
     * Generate the output from the entries contained in the database in a
     * output div in the form of JSON.
     * 
     * @param ids The ids of the entries retrieved.
     * @param titles The titles of the entries retrieved.
     * @param dates The dates each entry was posted.
     * @param times The time of day each entry was posted.
     * @param urls The urls of the entries retrieved.
     * @param authors The authors of the entries retrieved.
     * @return The output of the entries in JSON format.
     */
    private static String generateOutput(int ids[], String titles[], String dates[], String times[], String urls[], String authors[], String descriptions[])
    {
		String output = "[";
    	
		for (int i = 0; i < ids.length; i++)
		{
			output +=
					"{ " +
						"\"id\": "       + ids[i]     + ", " +
						"\"title\": \""  + titles[i]  + "\", " +
						"\"date\": \""   + dates[i]   + "\", " +
						"\"time\": \""   + times[i]   + "\", " +
						"\"url\": \""    + urls[i]    + "\", " + 
						"\"author\": \"" + authors[i] + "\", " +
						"\"description\": \"" + descriptions[i] + "\" " + 
					"}, ";
		}
		
		if (ids.length > 0)
		{
			output = output.substring(0, output.length() - 2);
		}
		
		output += " ]";
		
		return output;
    }
}