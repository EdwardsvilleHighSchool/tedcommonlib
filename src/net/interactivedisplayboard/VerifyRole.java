package net.interactivedisplayboard;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.interactivedisplayboard.database.DBConnection;
import net.interactivedisplayboard.logger.Log;
import net.interactivedisplayboard.logger.Logger;
import net.interactivedisplayboard.util.UserUtils;

// The URL in which the servlet is located at. e.g. http://localhost/addEntry
@WebServlet(urlPatterns = { "/verifyRole" })
/**
 * Class used to verify the credentials of a user.
 * 
 * Date: 2/24/2014 6:00:00 PM
 */
public class VerifyRole extends EHSServlet
{
    /**
     * Method called whenever a get request has been made on the
     * upload servlet.
     * 
     * @param request The request information from the sender.
     * @param response The response information to send back.
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        request.getRequestDispatcher("/error404").forward(request, response);
    }
    
    /**
     * Method called whenever a post request has been made on the
     * retrieve entry servlet.
     * 
     * @param request The request information from the sender.
     * @param response The response information to send back.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	String  rank      = request.getParameter("rank");
    	
    	boolean usingRank = false;
    	
    	if (rank != null)
    	{
    		rank      = rank.toLowerCase();
    		
    		usingRank = rank.equals("true");
    	}
    	
    	HttpSession session = request.getSession();
    	
    	ServletOutputStream out = response.getOutputStream();
    	
    	if (session == null)
    	{
    		if (usingRank)
        	{
    			out.print(0);
    			
    			return;
        	}
    		
    		out.print(false);
    	}
    	
    	String role      = request.getParameter("role");
    	
    	int requiredRank = UserUtils.getRank(role);
    	int currentRank  = (Integer)session.getAttribute("rank");
    	
    	if (currentRank >= requiredRank)
    	{
    		if (usingRank)
        	{
        		out.print(currentRank);
        		
        		return;
        	}
    		
    		out.print(true);
    	}
    }
}
