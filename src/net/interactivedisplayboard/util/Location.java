package net.interactivedisplayboard.util;

import java.io.File;

public class Location
{
	/**
	 * /home/ehs/workspace/TEDCommonLib/
	 */
	private static String getWebRoot()
	{
		File classFile = new File(Location.class.getProtectionDomain().getCodeSource().getLocation().getFile());
		
		File current   = classFile.getParentFile();
		
		while (!current.getName().equals("WebContent"))
		{
			current = current.getParentFile();
		}
		
		String location = current.getParentFile().getParent();
		
		return location;
	}
	
	/**
	 * /home/ehs/workspace/TEDControlPanel/
	 */
	public static String getControlPanelRoot()
	{
		return getWebRoot() + "/TEDControlPanel";
	}
	
	/**
	 * /home/ehs/workspace/InteractiveDisplayBoard/
	 */
	public static String getInteractiveDisplayBoardRoot()
	{
		return getWebRoot() + "/TED";
	}
	
	/**
	 * /home/ehs/workspace/TEDControlPanel/WebContent/
	 */
	public static String getControlPanelContentRoot()
	{
		return getControlPanelRoot() + "/WebContent";
	}
	
	/**
	 * /home/ehs/workspace/InteractiveDisplayBoard/WebContent/
	 */
	public static String getInteractiveDisplayBoardContentRoot()
	{
		return getInteractiveDisplayBoardRoot() + "/WebContent";
	}
	
	/**
	 * Replace a directory name in the given path String.<br>
	 * For example:
	 * <blockquote><pre>
	 * String path = "/path/to/changethis/directory/here";
	 * 
	 * String value = replaceDirectoryName(path, "changethis", "newDir");</pre></blockquote>
	 * The above value String would have the contents of:
	 * "/path/to/newDir/directory/here"
	 * 
	 * @param path The path to use as the source String to change a
	 * 		directory name in.
	 * @param oldName The old directory name to change.
	 * @param newName The new directory name to update the old one as.
	 * @return The newly generated String path.
	 */
	public static String replaceDirectoryName(String path, String oldName, String newName)
	{
		if (path.indexOf(oldName + '/') >= 0)
		{
			return path.replace(oldName + '/', newName + '/');
		}
		else if (path.indexOf('/' + oldName) >= 0)
		{
			return path.replace('/' + oldName, '/' + newName);
		}
		else if (path.equals(oldName))
		{
			return newName;
		}
		
		return path;
	}
}