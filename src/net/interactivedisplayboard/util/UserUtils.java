package net.interactivedisplayboard.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.interactivedisplayboard.database.DBConnection;

/**
 * 
 * 
 * @author Braden Steffaniak
 * 
 * Date: 5/20/2014 7:10:00 PM
 */
public class UserUtils
{
	private static final HashMap<String, Integer>	hierarchy;
	
	public  static final int	ADMIN = 3, TEACHER = 2, STUDENT = 1;
	
	static
	{
		hierarchy = new HashMap<String, Integer>();
		
		hierarchy.put("admin", ADMIN);
		hierarchy.put("teacher", TEACHER);
		hierarchy.put("student", STUDENT);
	}
	
	public static String getUsername(String username)
	{
		DBConnection authority = DBConnection.getAuthorityConnection();
		
		try
		{
			ResultSet user = authority.query("SELECT user_name FROM user_roles WHERE user_name = '" + username + "'");
		
			user.next();
			
			if (!user.isAfterLast())
			{
				String name = user.getString("user_name");
				
				return name;
			}
			
			return username;
		}
		catch (SQLException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	public static int getRank(String role)
	{
		role = role.toLowerCase();
		
		if (!hierarchy.containsKey(role))
		{
			throw new RuntimeException("Role '" + role + "' is not valid.");
		}
		
		return hierarchy.get(role);
	}
	
	public static int getHighestRank(String username)
	{
		DBConnection authority = DBConnection.getAuthorityConnection();
		
		try
		{
			ResultSet user = authority.query("SELECT role_name FROM user_roles WHERE user_name = '" + username + "'");
    		
			user.next();
			
    		int authorityRank = 0;
    		
			// Users may have multiple roles, so we need to check
    		// all of theirs roles for a match.
    		while (!user.isAfterLast())
    		{
    			String authorityRole = user.getString("role_name").toLowerCase();
    			
    			if (hierarchy.containsKey(authorityRole))
    			{
    				int rank = hierarchy.get(authorityRole);
    				
    				if (rank > authorityRank)
    				{
    					authorityRank = rank;
    				}
    			}
    			
    			user.next();
    		}
    		
    		return authorityRank;
		}
		catch (SQLException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Check whether or not the user with the specified username
	 * and encrypted password has the authorization of the given
	 * role.
	 * 
	 * @param username The name of the user to authorize.
	 * @param role The name of the role to check for authorization of.
	 * @return Whether or not the user is authorized for the given role.
	 */
	public static boolean checkAuthorization(String username, String role)
	{
		DBConnection authority = DBConnection.getAuthorityConnection();
		
		role = role.toLowerCase();
		
		try
		{
	    	ResultSet set = authority.query("SELECT role_name FROM user_roles WHERE user_name = '" + username + "'");

	    	if (DBConnection.getNumRows(set) > 0)
	    	{
	    		// Users may have multiple roles, so we need to check
	    		// all of theirs roles for a match.
	    		while (!set.isAfterLast())
	    		{
	    			set.next();
	    			
	    			if (set.getString("role_name").equals(role))
	    			{
	    				return true;
	    			}
	    		}
	    	}
		}
		catch (SQLException e)
		{
			throw new RuntimeException(e);
		}
		
		return false;
	}
	
	public static boolean registerUser(String username, String password)
	{
		return registerUser(username, password, "student", null);
	}
	
	/**
	 * 
	 * 
	 * @param username 
	 * @param password 
	 * @param role 
	 * @param authorityName 
	 * @return 
	 * @throws SQLException 
	 */
	public static boolean registerUser(String username, String password, String role, String authorityName)
	{
		DBConnection authority = DBConnection.getAuthorityConnection();
		
		if (authority == null)
		{
			return false;
		}
		
		try
		{
			if (!isAuthorizedToRegister(authorityName, role))
			{
				return false;
			}
			
			ResultSet user = authority.query("SELECT user_name FROM users WHERE user_name = '" + username + "'");
			
			// If a user with that name already exists.
			if (DBConnection.getNumRows(user) > 0)
			{
				return false;
			}
			
			// If the username or password's lengths are out of range.
			if (password.length() >= 128 || username.length() > 25 || password.length() <= 0 || username.length() <= 0)
			{
				return false;
			}
			
			authority.query("INSERT INTO users VALUES('" + username + "', '" + password + "');");
			authority.query("INSERT INTO user_roles VALUES('" + username + "', '" + role + "');");
		}
		catch (SQLException e)
		{
			return false;
		}
		
		return true;
	}
	
	private static boolean isAuthorizedToRegister(String username, String role)
	{
		if (username == null)
		{
			return false;
		}
		
		// Make the string matching case-insensitive.
		role = role.toLowerCase();
		
		int roleRank      = 0;
		int authorityRank = getHighestRank(username);
		
		if (hierarchy.containsKey(role))
		{
			roleRank = hierarchy.get(role);
		}
		
		// If the rank of the authority is higher than the
		// generated role.
		if (authorityRank > roleRank)
		{
			return true;
		}
		
		return false;
	}
	
	public static boolean validCredentials(String username, String password)
	{
		DBConnection users = DBConnection.getAuthorityConnection();
		
		try
		{
			ResultSet results = users.query("SELECT user_name FROM users WHERE user_name = '" + username + "' AND user_pass = '" + password + "';");
			
			// Return true if there is exactly one user with the
			// given credentials.
			return DBConnection.getNumRows(results) == 1;
		}
		catch (SQLException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	public static String getCurrentUsername(HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		
		if (session == null)
		{
			return null;
		}
		
    	return (String)session.getAttribute("username");
	}
	
	public static int getCurrentRank(HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		
		if (session == null)
		{
			return 0;
		}
		
    	return (Integer)session.getAttribute("rank");
	}
	
	public static boolean isLoggedIn(HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		
		return session != null;
	}
}